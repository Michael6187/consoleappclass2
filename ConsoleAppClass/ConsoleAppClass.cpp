﻿#include <iostream>
using namespace std;

class Animal
{
public:
	Animal()
	{

	}
	virtual void Voice() { cout << "=====" << endl; }
};

class Dog : public Animal
{
public:
	Dog()
	{

	}
	void Voice() override { cout << "Woof!" << endl; }
};

class Cat : public Animal
{
	void Voice() override { cout << "Meow!" << endl; }
};

class Monkey : public Animal
{
	void Voice() override { cout << "y aa yya aaa yyaa oooo aaaaa!" << endl; }
};

int main()
{
	Dog* D1 = new Dog;
	Cat* C1 = new Cat;
	Monkey* M1 = new Monkey;
	Dog* D2 = new Dog;
	Cat* C2 = new Cat;
	const int size = 5;
	Animal* array[size] = { D1, C1, M1, D2, C2 };

	for (int i = 0; i < size; i++)
	{
		array[i]->Voice();
	}



	//Dog* Dog1;
	//Dog* Dog2;
	//Cat* Cat1;
	//Cat* Cat2;
	//Monkey* Monkey1;
	//const int size = 5;
	//Animal* array[size] = { Dog1 };

	/*{
		Dog1,
		Dog2,
		Cat1,
		Cat2,
		Monkey1
	};*/




	/*
	array[0] = Dog1;
	array[1] = Cat1;
	array[2] = Monkey1;
	array[3] = Cat2;
	array[4] = Dog2;
	*/

	//for (int i = 0; i < size; i++)
	//{
	//	array[i]->Voice();
	//}

}